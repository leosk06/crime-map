package com.leolei.crimemap;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leolei.crimemap.model.CrimeReport;

import java.util.ArrayList;
import java.util.List;

public class RawDataAdapter extends RecyclerView.Adapter<RawDataAdapter.ViewHolder> {
    private static final int sViewTypeReportRow = 0;
    private static final int sViewTypeLoadingMore = 1;

    private List<CrimeReport> mData;
    private boolean mShowLoadingMore = false;

    public interface OnItemClickListener {
        void onItemClick(CrimeReport item);
    }
    private OnItemClickListener mOnItemClickListener;

    private class OnClickListener implements View.OnClickListener {
        private final CrimeReport mCrimeReport;

        public OnClickListener(CrimeReport report) {
            mCrimeReport = report;
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(mCrimeReport);
            }
        }
    }

    public RawDataAdapter() {
        mData = new ArrayList<>();
    }

    public void addAll(List<CrimeReport> moreData) {
        mData.addAll(moreData);
        notifyDataSetChanged();
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view;
        if (viewType == sViewTypeLoadingMore) {
            view = inflater.inflate(R.layout.layout_load_more_indicator, parent, false);
        } else {
            view = inflater.inflate(R.layout.layout_crime_report, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, int position) {
        if (mShowLoadingMore && position == mData.size()) {
            return;
        }

        CrimeReport item = mData.get(position);
        vh.mDescription.setText(item.getDescription());
        vh.mDistrict.setText(item.getDistrict());
        vh.mResolution.setText(item.getResolution());
        vh.mOffenseType.setText(item.getOffenseType());
        vh.mDate.setText(Utils.formatServerDate(vh.mDate.getContext(), item.getDate()));

        vh.itemView.setOnClickListener(new OnClickListener(item));
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mData.size()) {
            return sViewTypeLoadingMore;
        }
        return sViewTypeReportRow;
    }

    @Override
    public int getItemCount() {
        return mData.size() + (mShowLoadingMore? 1 : 0);
    }

    public void showLoadingMore(boolean show) {
        mShowLoadingMore = show;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onClickListener) {
        mOnItemClickListener = onClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDate;
        public TextView mDistrict;
        public TextView mOffenseType;
        public TextView mDescription;
        public TextView mResolution;

        public ViewHolder(View view) {
            super(view);
            mDate = (TextView) view.findViewById(R.id.txt_date);
            mDistrict = (TextView) view.findViewById(R.id.txt_district);
            mOffenseType = (TextView) view.findViewById(R.id.txt_offense_type);
            mDescription = (TextView) view.findViewById(R.id.txt_description);
            mResolution = (TextView) view.findViewById(R.id.txt_resolution);
        }
    }
}
