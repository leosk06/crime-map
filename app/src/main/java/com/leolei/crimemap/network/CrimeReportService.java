package com.leolei.crimemap.network;

import com.leolei.crimemap.model.CrimeReport;
import com.leolei.crimemap.model.District;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class CrimeReportService {
    public static final SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static CrimeReportService sInstance = null;

    private CrimeReportServiceInternal mInternalWeService;
    ArrayList<NetworkListener> mListeners = new ArrayList<> ();

    private CrimeReportService() {
    }

    public static CrimeReportService getInstance() {
        if (sInstance == null) {
            sInstance = new CrimeReportService();
        }
        return sInstance;
    }

    public CrimeReportService(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mInternalWeService = retrofit.create(CrimeReportServiceInternal.class);
    }

    public void getDistrictAggregateReportCounts(Date from, Date until, ServiceCallback callback) {
        Call<List<District>> call = mInternalWeService.getDistrictAggregateReportCounts(whereDate(from, until));
        call.enqueue(new ServiceCallbackInternal<List<District>>(callback));
    }

    public void getRawCrimeReports(Date from, Date until, long limit, long offset, ServiceCallback callback) {
        Call<List<CrimeReport>> call = mInternalWeService.getRawCrimeReports(whereDate(from, until), limit, offset);
        call.enqueue(new ServiceCallbackInternal<List<CrimeReport>>(callback));
    }


    public void addEventListener(NetworkListener listener) {
        mListeners.add(listener);
    }

    public void removeEventListener(NetworkListener listener) {
        mListeners.remove(listener);
    }

    private String whereDate(Date from, Date until) {
        return String.format("date >= '%s' AND date <= '%s'", sDateFormat.format(from), sDateFormat.format(until));
    }
}
