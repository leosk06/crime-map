package com.leolei.crimemap.model;

import android.os.Parcel;
import android.os.Parcelable;

public class District implements Parcelable {
    private String pddistrict;
    private long count_time;
    private double avg_sub_col_location_latitude;
    private double avg_sub_col_location_longitude;

    public String getName() {
        return pddistrict;
    }

    public long getReportCount() {
        return count_time;
    }

    public double getLatitude() {
        return avg_sub_col_location_latitude;
    }

    public double getLongitude() {
        return avg_sub_col_location_longitude;
    }

    /*** PARCELABLE ***/
    private District(Parcel in) {
        pddistrict = in.readString();
        count_time = in.readLong();
        avg_sub_col_location_latitude = in.readDouble();
        avg_sub_col_location_longitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pddistrict);
        dest.writeLong(count_time);
        dest.writeDouble(avg_sub_col_location_latitude);
        dest.writeDouble(avg_sub_col_location_longitude);
    }

    public static final Creator<District> CREATOR = new Creator<District>() {
        @Override
        public District createFromParcel(Parcel in) {
            return new District(in);
        }

        @Override
        public District[] newArray(int size) {
            return new District[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
