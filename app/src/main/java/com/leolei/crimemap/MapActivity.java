package com.leolei.crimemap;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.leolei.crimemap.model.CrimeReport;
import com.leolei.crimemap.model.District;

import java.util.List;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String sParamDistricts = "ParamDistricts";
    public static final String sReportPoint = "ParamOtherPoints";

    private GoogleMap mMap;
    private View mProgressIndicator;

    private List<District> mDistricts;
    private CrimeReport mReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mProgressIndicator = findViewById(R.id.progressIndicator);

        setupActionBar();
        getMapPointsFromExtras();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        showProgressIndicator();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDistricts != null && mMap != null) {
            focusMapOnPoints();
        }
    }

    private void getMapPointsFromExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mDistricts = extras.getParcelableArrayList(sParamDistricts);
            mReport = extras.getParcelable(sReportPoint);
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        final LatLng sfo = new LatLng(37.7576793, -122.5076405);
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sfo));
        markDistrictsOnMap();
        markOtherPointOnMap();
        focusMapOnPoints();
        hideProgressIndicator();
    }

    private void markOtherPointOnMap() {
        if (mReport == null) {
            return;
        }

        float markerHue = Utils.rgbToHsv(Color.parseColor("#607b8d"))[0];
        LatLng point = new LatLng(mReport.getLatitude(), mReport.getLongitude());

        String title = String.format("%s - %s",
                Utils.formatServerDate(this, mReport.getDate()), mReport.getOffenseType());
        String snippet = "Resolution: " + mReport.getResolution();

        MarkerOptions marker = new MarkerOptions()
                .position(point)
                .icon(BitmapDescriptorFactory.defaultMarker(markerHue))
                .snippet(snippet)
                .title(title);
        mMap.addMarker(marker);
    }

    private void markDistrictsOnMap() {
        if (mDistricts == null) {
            return;
        }

        final int[] markerColors = getResources().getIntArray(R.array.markerColors);

        // Divide districts as equally as possible in as many groups as color markers we have.
        // Both districts and color markers are in ascending order
        float step = markerColors.length / (float) mDistricts.size();

        for (int i = 0, I = mDistricts.size(); i < I; i++) {
            District district = mDistricts.get(i);

            int colorIndex = (int) Math.floor(i / step);
            int markerColor = markerColors[colorIndex];
            float markerHue = Utils.rgbToHsv(markerColor)[0];

            LatLng point = new LatLng(district.getLatitude(), district.getLongitude());
            MarkerOptions marker = new MarkerOptions()
                    .position(point)
                    .icon(BitmapDescriptorFactory.defaultMarker(markerHue))
                    .snippet(String.format("%d reported incidents", district.getReportCount()))
                    .title(district.getName());
            mMap.addMarker(marker);
        }
    }

    private void focusMapOnPoints() {
        final LatLngBounds.Builder latLngBoundBuilder = new LatLngBounds.Builder();

        if (mDistricts != null) {
            for (int i = 0, I = mDistricts.size(); i < I; i++) {
                District district = mDistricts.get(i);
                LatLng point = new LatLng(district.getLatitude(), district.getLongitude());
                latLngBoundBuilder.include(point);
            }
        }

        if (mReport != null) {
            latLngBoundBuilder.include(new LatLng(mReport.getLatitude(), mReport.getLongitude()));
        }

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                centerMapOnPoints(latLngBoundBuilder);
            }
        });
    }

    private void centerMapOnPoints(LatLngBounds.Builder latLngBoundBuilder) {
        try {
            mMap.moveCamera(
                    CameraUpdateFactory.newLatLngBounds(
                            latLngBoundBuilder.build(),
                            getResources().getDimensionPixelSize(R.dimen.centered_map_padding)
                    )
            );
        } catch (IllegalStateException e) {
            // no included points
            e.printStackTrace();
        }
    }

    protected void showProgressIndicator() {
        mProgressIndicator.setVisibility(View.VISIBLE);
    }

    protected void hideProgressIndicator() {
        mProgressIndicator.setVisibility(View.INVISIBLE);
    }
}
