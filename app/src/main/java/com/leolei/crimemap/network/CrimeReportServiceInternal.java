package com.leolei.crimemap.network;

import com.leolei.crimemap.model.CrimeReport;
import com.leolei.crimemap.model.District;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

// Service endpoints description for Retrofit
interface CrimeReportServiceInternal {
    //TODO: Please not that the webservice seems to have no reports from 2015-12-31 and later
    @GET("/resource/ritf-b9ki.json?$select=pddistrict,COUNT(time),AVG(location.latitude),AVG(location.longitude)" +
            "&$group=pddistrict&$order=count_time ASC")
    Call<List<District>> getDistrictAggregateReportCounts(@Query("$where") String where);

    @GET("/resource/ritf-b9ki.json?$select=category,pddistrict,descript,resolution,date,location.latitude,location.longitude&$order=date DESC")
    Call<List<CrimeReport>> getRawCrimeReports(
            @Query("$where") String where,
            @Query("$limit") long limit,
            @Query("$offset") long offset
    );
}

