package com.leolei.crimemap;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class InfiniteScrollListener extends RecyclerView.OnScrollListener {
    private static final int sLookAhead = 5;

    private LinearLayoutManager mLayoutManager;

    private int mVisibleItems;
    private int mFirstVisibleItem;
    private int mTotalItems;
    private int mPreviousTotalItems;

    private boolean mLoading = true;

    public InfiniteScrollListener(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    public abstract void onBottomReached();

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        // Ignore events that won't take us to the bottom
        if (dy <= 0) {
            return;
        }

        mFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
        mVisibleItems = mLayoutManager.getChildCount();
        mTotalItems = mLayoutManager.getItemCount();

        if (mLoading) {
            if (hasFinishedLoading()) {
                mLoading = false;
                mPreviousTotalItems = mTotalItems;
            } else if (isRefreshing()) {
                mPreviousTotalItems = 0;
            }
        }

        if (!mLoading && isAboutToReachBottom()) {
            mLoading = true;
            onBottomReached();
        }
    }

    private boolean hasFinishedLoading() {
        return mTotalItems > mPreviousTotalItems;
    }

    private boolean isRefreshing() {
        return mTotalItems < mPreviousTotalItems;
    }

    private boolean isAboutToReachBottom() {
        return mTotalItems <= mFirstVisibleItem + mVisibleItems + sLookAhead;
    }
}
