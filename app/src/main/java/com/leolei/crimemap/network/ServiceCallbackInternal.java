package com.leolei.crimemap.network;

import android.util.Log;

import java.io.IOException;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

// Retrofit callback that executes the users ServiceCallback, if its originating NetworkListener is still listening
class ServiceCallbackInternal<T> implements Callback<T> {
    private static final String sErrorMsg = "Unexpected network error, please try again later";

    private ServiceCallback mServiceCallback;

    public ServiceCallbackInternal(ServiceCallback callback) {
        mServiceCallback = callback;
    }

    @Override
    public void onResponse(Response response, Retrofit retrofit) {
        if (!CrimeReportService.getInstance().mListeners.contains(mServiceCallback.mListener)) {
            return;
        }

        if (response.body() != null) {
            mServiceCallback.onSuccess(response.body());
        } else if (response.errorBody() != null) {
            try {
                Log.e("Crime Map", response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mServiceCallback.onError(sErrorMsg);
            }
        }
    }

    @Override
    public void onFailure(Throwable t) {
        t.printStackTrace();
        if (CrimeReportService.getInstance().mListeners.contains(mServiceCallback.mListener)) {
            mServiceCallback.onError(sErrorMsg);
        }
    }
}
