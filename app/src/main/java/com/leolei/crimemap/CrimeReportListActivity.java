package com.leolei.crimemap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.leolei.crimemap.model.CrimeReport;
import com.leolei.crimemap.model.District;
import com.leolei.crimemap.network.CrimeReportService;
import com.leolei.crimemap.network.NetworkListener;
import com.leolei.crimemap.network.ServiceCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrimeReportListActivity extends AppCompatActivity implements NetworkListener {
    private static final long sResultsPerPage = 30;

    private CrimeReportService mWebService;

    private SwipeRefreshLayout mSwipeRefresh;
    private RecyclerView mRecyclerView;
    private RawDataAdapter mAdapter;

    private long mOffset = 0;
    private ArrayList<District> mDistricts;

    // Maximum date to be updated when running a query for the first time, or on refresh,
    // but not for listing other than page 1
    private Date mMaxListingDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raw_data);

        mWebService = new CrimeReportService(getString(R.string.service_base_url));

        setupRecyclerView();
        setupSwipeRefresh();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSwipeRefresh.setRefreshing(false);
        CrimeReportService.getInstance().addEventListener(this);
        if (mAdapter.getItemCount() == 0) {
            launchMapQuery();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        CrimeReportService.getInstance().removeEventListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_map:
                openMap(null);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        mAdapter = new RawDataAdapter();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new InfiniteScrollListener(layoutManager) {
            @Override
            public void onBottomReached() {
                mAdapter.showLoadingMore(true);
                // Here we don't update the maximum date, because we want a new page of the current listing,
                // which used the current date
                // This prevents duplicate data on the listing in case the webservice gets new crime reports
                // before our user reaches the listing bottom.
                launchCrimeReportDataQuery();
            }
        });
        mAdapter.setOnItemClickListener(new RawDataAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(CrimeReport item) {
                openMap(item);
            }
        });
    }

    private void openMap(CrimeReport item) {
        Intent i = new Intent();
        i.setClass(getApplicationContext(), MapActivity.class);
        i.putParcelableArrayListExtra(MapActivity.sParamDistricts, mDistricts);
        if (item != null) {
            i.putExtra(MapActivity.sReportPoint, item);
        }
        startActivity(i);
    }

    private void setupSwipeRefresh() {
        mSwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefresh.setRefreshing(true);
                refreshRawData();
            }
        });
    }

    private void refreshRawData() {
        mRecyclerView.scrollTo(0, 0);
        mAdapter.clear();
        mOffset = 0;
        updateMaxListingDate();
        launchCrimeReportDataQuery();
    }

    private void updateMaxListingDate() {
        mMaxListingDate = new Date();
    }

    @SuppressWarnings("unchecked")
    private void launchCrimeReportDataQuery() {
        Date from = Utils.oneMonthAgoDate(mMaxListingDate.getTime());

        mWebService.getRawCrimeReports(from, mMaxListingDate, sResultsPerPage, mOffset, new ServiceCallback(this) {
            @Override
            public void onSuccess(Object response) {
                List<CrimeReport> crimeReports = (List<CrimeReport>) response;
                mSwipeRefresh.setRefreshing(false);
                mAdapter.showLoadingMore(false);
                mOffset += crimeReports.size();
                mAdapter.addAll(crimeReports);
            }

            @Override
            public void onError(String s) {
                mSwipeRefresh.setRefreshing(false);
                mAdapter.showLoadingMore(false);
                errorMessage(s);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void launchMapQuery() {
        updateMaxListingDate();
        Date from = Utils.oneMonthAgoDate(mMaxListingDate.getTime());
        mWebService.getDistrictAggregateReportCounts(from, mMaxListingDate, new ServiceCallback(this) {
            @Override
            public void onSuccess(Object response) {
                mDistricts = new ArrayList<>((List<District>) response);
                // We don't udpate date here, as it was already done on the Map query
                launchCrimeReportDataQuery();
            }

            @Override
            public void onError(String s) {
                errorMessage(s);
            }
        });
    }

    protected void errorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
