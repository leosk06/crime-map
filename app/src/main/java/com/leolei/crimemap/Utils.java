package com.leolei.crimemap;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateUtils;

import com.leolei.crimemap.network.CrimeReportService;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static android.text.format.DateUtils.FORMAT_ABBREV_ALL;
import static android.text.format.DateUtils.FORMAT_SHOW_DATE;
import static android.text.format.DateUtils.FORMAT_SHOW_WEEKDAY;
import static android.text.format.DateUtils.FORMAT_SHOW_YEAR;

final class Utils {
    private Utils() {
    }

    public static Date oneMonthAgoDate(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

    public static float[] rgbToHsv(int rgbColor) {
        float[] hsv = new float[3];
        Color.colorToHSV(rgbColor, hsv);
        return hsv;

    }

    public static String formatServerDate(Context context, String dateStr) {
        Date date;
        try {
            date = CrimeReportService.sDateFormat.parse(dateStr);
            return DateUtils.formatDateTime(context, date.getTime(),
                    FORMAT_SHOW_WEEKDAY | FORMAT_SHOW_DATE | FORMAT_SHOW_YEAR | FORMAT_ABBREV_ALL);
        } catch (ParseException e) {
            e.printStackTrace();
            return context.getString(R.string.error_unknown_date);
        }
    }
}
