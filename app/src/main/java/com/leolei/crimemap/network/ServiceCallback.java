package com.leolei.crimemap.network;

// Service callback for the end user
public abstract class ServiceCallback {
    NetworkListener mListener;

    public ServiceCallback(NetworkListener listener) {
        mListener = listener;
    }

    public abstract void onSuccess(Object response);
    public abstract void onError(String s);
}
