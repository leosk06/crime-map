package com.leolei.crimemap.model;


import android.os.Parcel;
import android.os.Parcelable;

public class CrimeReport implements Parcelable {
    private String category;
    private String pddistrict;
    private String descript;
    private String resolution;
    private String date;
    private double sub_col_location_latitude;
    private double sub_col_location_longitude;

    public String getOffenseType() {
        return category;
    }

    public String getDistrict() {
        return pddistrict;
    }

    public String getDescription() {
        return descript;
    }

    public String getResolution() {
        return resolution;
    }

    public String getDate() {
        return date;
    }

    public double getLatitude() {
        return sub_col_location_latitude;
    }

    public double getLongitude() {
        return sub_col_location_longitude;
    }

    /*** PARCELABLE ***/
    private CrimeReport(Parcel in) {
        category = in.readString();
        pddistrict = in.readString();
        descript = in.readString();
        resolution = in.readString();
        date = in.readString();
        sub_col_location_latitude = in.readDouble();
        sub_col_location_longitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(pddistrict);
        dest.writeString(descript);
        dest.writeString(resolution);
        dest.writeString(date);
        dest.writeDouble(sub_col_location_latitude);
        dest.writeDouble(sub_col_location_longitude);
    }

    public static final Creator<CrimeReport> CREATOR = new Creator<CrimeReport>() {
        @Override
        public CrimeReport createFromParcel(Parcel in) {
            return new CrimeReport(in);
        }

        @Override
        public CrimeReport[] newArray(int size) {
            return new CrimeReport[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

}
